async function getToken() {
  const form = document.forms.login;
  const emailElem = form.email;
  const passElem = form.password;

  try {
    let response = await fetch(
      "https://ajax.test-danit.com/api/v2/cards/login",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: `${emailElem.value}`,
          password: `${passElem.value}`,
        }),
      }
    );

    if (response.ok) {
      const token = await response.text();
      console.log("access opened");
      console.log(token);
      localStorage.setItem("token", token);
    } else {
      console.error("Login failed");
      alert("Authorization is failed, please check email or password !");
      localStorage.removeItem("token");
    }
  } catch (err) {
    alert(err.message);
  }
}

class Visit {
  constructor(doctor, purpose, description, priority, name, id) {
    this.doctor = doctor;
    this.purpose = purpose;
    this.description = description;
    this.priority = priority;
    this.name = name;
    this.id = id;
  }

  render() {
    const cardElement = document.createElement("div");
    cardElement.classList.add("card");
    cardElement.innerHTML = `
				<h3>Name: ${this.name}</h3>				
				<p>Doctor: ${this.doctor}</p>
                <button type="button" class="btn-close card-button" aria-label="Delete"></button>
                <button type="button" class="btn btn-secondary show-more-button">Show More</button>
                <div class="more-data mt-3" style="display: none;">
                    <p>Purpose of visit: ${this.purpose}</p>
                    <p>Description of visit: ${this.description}</p>
                    <p>Priority: ${this.priority}</p>
                    <p>ID: ${this.id}</p>
                    <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#edit-visit-dentist">Edit</button>
                </div>
			`;
    const deleteBtn = cardElement.querySelector(".card-button");
    deleteBtn.addEventListener("click", () => {
      this.deleteCard();
    });

    const editBtn = cardElement.querySelector(".btn.btn-warning");
    editBtn.addEventListener("click", () => {
      this.openEditModal();
    });

    const showMoreBtn = cardElement.querySelector(".show-more-button");
    const moreData = cardElement.querySelector(".more-data");

    showMoreBtn.addEventListener("click", () => {
      if (moreData.style.display === "none") {
        moreData.style.display = "block";
        showMoreBtn.textContent = "Show Less";
      } else {
        moreData.style.display = "none";
        showMoreBtn.textContent = "Show More";
      }
    });

    return cardElement;
  }

  async deleteCard() {
    try {
      const response = await fetch(
        `https://ajax.test-danit.com/api/v2/cards/${this.id}`,
        {
          method: "DELETE",
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );

      if (response.ok) {
        const cardElement = document.querySelector(`[data-id="${this.id}"]`);
        cardElement.remove();
      } else {
        console.error("Error:", response.status);
      }
    } catch (error) {
      console.error("Error:", error);
    }
  }
}

class VisitCardiologist extends Visit {
  constructor(
    doctor,
    purpose,
    description,
    priority,
    name,
    id,
    pressure,
    bodyMassIndex,
    heart,
    age
  ) {
    super(doctor, purpose, description, priority, name, id);
    this.pressure = pressure;
    this.bodyMassIndex = bodyMassIndex;
    this.heart = heart;
    this.age = age;
  }

  openEditModal() {
    const inputDoctor = document.getElementById("doctor");
    const inputPurpose = document.getElementById("purpose");
    const inputDescription = document.getElementById("description");
    const inputPriority = document.getElementById("priority");
    const inputName = document.getElementById("name");
    const inputPressure = document.getElementById("pressureCardiologist");
    const inputBodyMassIndex = document.getElementById(
      "bodyMassIndexCardiologist"
    );
    const inputHeart = document.getElementById("heartCardiologist");
    const inputAge = document.getElementById("ageCardiologist");

    inputDoctor.value = this.doctor;
    inputPurpose.value = this.purpose;
    inputDescription.value = this.description;
    inputPriority.value = this.priority;
    inputName.value = this.name;
    inputPressure.value = this.pressure;
    inputBodyMassIndex.value = this.bodyMassIndex;
    inputHeart.value = this.heart;
    inputAge.value = this.age;

    const form = document.getElementById("editForm");
    form.addEventListener("submit", (ev) => {
      // ev.preventDefault();
      this.saveCardChanges();
      let modal = document.getElementById("edit-visit");
      let modalInstance = bootstrap.Modal.getInstance(modal); // Отримати екземпляр модального окна
      modalInstance.hide();
      editForm.reset();
    });
  }

  async saveCardChanges() {
    try {
      const inputDoctor = document.getElementById("doctor");
      const inputPurpose = document.getElementById("purpose");
      const inputDescription = document.getElementById("description");
      const inputPriority = document.getElementById("priority");
      const inputName = document.getElementById("name");
      const inputPressure = document.getElementById("pressureCardiologist");
      const inputBodyMassIndex = document.getElementById(
        "bodyMassIndexCardiologist"
      );
      const inputHeart = document.getElementById("heartCardiologist");
      const inputAge = document.getElementById("ageCardiologist");

      const response = await fetch(
        `https://ajax.test-danit.com/api/v2/cards/${this.id}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
          body: JSON.stringify({
            id: this.id,
            doctor: inputDoctor.value,
            purpose: inputPurpose.value,
            description: inputDescription.value,
            priority: inputPriority.value,
            name: inputName.value,
            pressure: inputPressure.value,
            bodyMassIndex: inputBodyMassIndex.value,
            heart: inputHeart.value,
            age: inputAge.value,
          }),
        }
      );
    } catch (error) {
      console.error("Error:", error);
    }
  }

  render() {
    const cardElement = document.createElement("div");
    cardElement.classList.add("card");
    cardElement.innerHTML = `
				<h3>Name: ${this.name}</h3>				
				<p>Doctor: ${this.doctor}</p>
                <button type="button" class="btn-close card-button" aria-label="Delete"></button>
                <button type="button" class="btn btn-secondary show-more-button">Show More</button>
                <div class="more-data mt-3" style="display: none;">
                    <p>Purpose of visit: ${this.purpose}</p>
                    <p>Description of visit: ${this.description}</p>
                    <p>Priority: ${this.priority}</p>
                    <p>Pressure: ${this.pressure}</p>
                    <p>Body mass index: ${this.bodyMassIndex}</p>
                    <p>Diseases of heart: ${this.heart}</p>
                    <p>Age: ${this.age}</p>
                    <p>ID: ${this.id}</p>				
                    <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#edit-visit" >Edit</button>
                </div>
				
			`;
    const deleteBtn = cardElement.querySelector(".btn-close, .card-button");
    deleteBtn.addEventListener("click", () => {
      this.deleteCard();
    });

    const editBtn = cardElement.querySelector(".btn.btn-warning");
    editBtn.addEventListener("click", () => {
      this.openEditModal();
    });

    const showMoreBtn = cardElement.querySelector(".show-more-button");
    const moreData = cardElement.querySelector(".more-data");

    showMoreBtn.addEventListener("click", () => {
      if (moreData.style.display === "none") {
        moreData.style.display = "block";
        showMoreBtn.textContent = "Show Less";
      } else {
        moreData.style.display = "none";
        showMoreBtn.textContent = "Show More";
      }
    });

    return cardElement;
  }
}

class VisitDentist extends Visit {
  constructor(doctor, purpose, description, priority, name, id, lastVisit) {
    super(doctor, purpose, description, priority, name, id);
    this.lastVisit = lastVisit;
  }

  openEditModal() {
    const inputDoctor = document.getElementById("doctorDentist");
    const inputPurpose = document.getElementById("purposeDentist");
    const inputDescription = document.getElementById("descriptionDentist");
    const inputPriority = document.getElementById("priorityDentist");
    const inputName = document.getElementById("nameDentist");
    const inputLastVisit = document.getElementById("lastVisitDentist");

    inputDoctor.value = this.doctor;
    inputPurpose.value = this.purpose;
    inputDescription.value = this.description;
    inputPriority.value = this.priority;
    inputName.value = this.name;
    inputLastVisit.value = this.lastVisit;

    const form = document.getElementById("editFormDentist");
    form.addEventListener("submit", (ev) => {
      // ev.preventDefault();
      this.saveCardChanges();
      let modal = document.getElementById("edit-visit-dentist");
      let modalInstance = bootstrap.Modal.getInstance(modal); // Отримати екземпляр модального окна
      modalInstance.hide();
      editForm.reset();
    });
  }

  async saveCardChanges() {
    try {
      const inputDoctor = document.getElementById("doctorDentist");
      const inputPurpose = document.getElementById("purposeDentist");
      const inputDescription = document.getElementById("descriptionDentist");
      const inputPriority = document.getElementById("priorityDentist");
      const inputName = document.getElementById("nameDentist");
      const inputLastVisit = document.getElementById("lastVisitDentist");

      const response = await fetch(
        `https://ajax.test-danit.com/api/v2/cards/${this.id}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
          body: JSON.stringify({
            id: this.id,
            doctor: inputDoctor.value,
            purpose: inputPurpose.value,
            description: inputDescription.value,
            priority: inputPriority.value,
            name: inputName.value,
            lastVisit: inputLastVisit.value,
          }),
        }
      );
    } catch (error) {
      console.error("Error:", error);
    }
  }

  render() {
    const cardElement = document.createElement("div");
    cardElement.classList.add("card");
    cardElement.innerHTML = `
				<h3>Name: ${this.name}</h3>				
				<p>Doctor: ${this.doctor}</p>
                <button type="button" class="btn-close card-button" aria-label="Delete"></button>
                <button type="button" class="btn btn-secondary show-more-button">Show More</button>
                <div class="more-data mt-3" style="display: none;">
                    <p>Purpose of visit: ${this.purpose}</p>
                    <p>Description of visit: ${this.description}</p>
                    <p>Priority: ${this.priority}</p>
                    <p>Last visit: ${this.lastVisit}</p>
                    <p>ID: ${this.id}</p>
                    <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#edit-visit-dentist">Edit</button>
                </div>
			`;
    const deleteBtn = cardElement.querySelector(".card-button");
    deleteBtn.addEventListener("click", () => {
      this.deleteCard();
    });

    const editBtn = cardElement.querySelector(".btn.btn-warning");
    editBtn.addEventListener("click", () => {
      this.openEditModal();
    });

    const showMoreBtn = cardElement.querySelector(".show-more-button");
    const moreData = cardElement.querySelector(".more-data");

    showMoreBtn.addEventListener("click", () => {
      if (moreData.style.display === "none") {
        moreData.style.display = "block";
        showMoreBtn.textContent = "Show Less";
      } else {
        moreData.style.display = "none";
        showMoreBtn.textContent = "Show More";
      }
    });

    return cardElement;
  }
}

class VisitTherapist extends Visit {
  constructor(doctor, purpose, description, priority, name, id, age) {
    super(doctor, purpose, description, priority, name, id);
    this.age = age;
  }

  openEditModal() {
    const inputDoctor = document.getElementById("doctorTherapist");
    const inputPurpose = document.getElementById("purposeTherapist");
    const inputDescription = document.getElementById("descriptionTherapist");
    const inputPriority = document.getElementById("priorityTherapist");
    const inputName = document.getElementById("nameTherapist");
    const inputAge = document.getElementById("ageTherapist");

    inputDoctor.value = this.doctor;
    inputPurpose.value = this.purpose;
    inputDescription.value = this.description;
    inputPriority.value = this.priority;
    inputName.value = this.name;
    inputAge.value = this.age;

    const form = document.getElementById("editFormTherapist");
    form.addEventListener("submit", (ev) => {
      // ev.preventDefault();
      this.saveCardChanges();
      let modal = document.getElementById("edit-visit-therapist");
      let modalInstance = bootstrap.Modal.getInstance(modal); // Отримати екземпляр модального окна
      modalInstance.hide();
      editForm.reset();
    });
  }

  async saveCardChanges() {
    try {
      const inputDoctor = document.getElementById("doctorTherapist");
      const inputPurpose = document.getElementById("purposeTherapist");
      const inputDescription = document.getElementById("descriptionTherapist");
      const inputPriority = document.getElementById("priorityTherapist");
      const inputName = document.getElementById("nameTherapist");
      const inputAge = document.getElementById("ageTherapist");

      const response = await fetch(
        `https://ajax.test-danit.com/api/v2/cards/${this.id}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
          body: JSON.stringify({
            id: this.id,
            doctor: inputDoctor.value,
            purpose: inputPurpose.value,
            description: inputDescription.value,
            priority: inputPriority.value,
            name: inputName.value,
            age: inputAge.value,
          }),
        }
      );
    } catch (error) {
      console.error("Error:", error);
    }
  }

  render() {
    const cardElement = document.createElement("div");
    cardElement.classList.add("card");
    cardElement.innerHTML = `
				<h3>Name: ${this.name}</h3>				
				<p>Doctor: ${this.doctor}</p>
                <button type="button" class="btn-close card-button" aria-label="Delete"></button>
                <button type="button" class="btn btn-secondary show-more-button">Show More</button>
                <div class="more-data mt-3" style="display: none;">
                    <p>Purpose of visit: ${this.purpose}</p>
                    <p>Description of visit: ${this.description}</p>
                    <p>Priority: ${this.priority}</p>
                    <p>Age: ${this.age}</p>
                    <p>ID: ${this.id}</p>
                    <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#edit-visit-therapist">Edit</button>
                </div>
			`;
    const deleteBtn = cardElement.querySelector(".card-button");
    deleteBtn.addEventListener("click", () => {
      this.deleteCard();
    });

    const editBtn = cardElement.querySelector(".btn.btn-warning");
    editBtn.addEventListener("click", () => {
      this.openEditModal();
    });

    const showMoreBtn = cardElement.querySelector(".show-more-button");
    const moreData = cardElement.querySelector(".more-data");

    showMoreBtn.addEventListener("click", () => {
      if (moreData.style.display === "none") {
        moreData.style.display = "block";
        showMoreBtn.textContent = "Show Less";
      } else {
        moreData.style.display = "none";
        showMoreBtn.textContent = "Show More";
      }
    });

    return cardElement;
  }
}

const visitForm = document.forms.visitForm;
const doctor = visitForm.doctor;
const form = document.forms.login;

if (localStorage.getItem("token")) {
  viewAllCards();
}

// get token
form.addEventListener("submit", (el) => {
  el.preventDefault();
  if (localStorage.getItem("token")) {
    const loginBtn = document.querySelector(".header__btn-login");
    const createBtn = document.querySelector(".header__btn-create-visit");
    const noitems = document.querySelector(".main__items-text");
    const welcome = document.querySelector(".main__text-wrapper");
    const cardsWrapper = document.querySelector(".main__cards-wrapper");

    loginBtn.classList.add("d-none");
    createBtn.classList.remove("d-none");
    noitems.classList.remove("d-none");
    welcome.classList.add("d-none");
    cardsWrapper.classList.remove("d-none");
  }

  async function login() {
    await getToken();
    await viewAllCards();
  }

  login();
});

// choose a doctor
doctor.addEventListener("change", () => {
  const cardiologist = document.querySelector(".cardiologist-options");
  const dentist = document.querySelector(".dentist-options");
  const therapist = document.querySelector(".therapist-options");
  const lastVisit = document.querySelector("#lastVisit");
  const age = document.querySelector("#age");
  const pressure = document.querySelector("#pressure");
  const bodyMassIndex = document.querySelector("#bodyMassIndex");
  const heart = document.querySelector("#heart");

  switch (doctor.value) {
    case "cardiologist":
      cardiologist.classList.remove("d-none");
      dentist.classList.add("d-none");
      therapist.classList.remove("d-none");
      lastVisit.removeAttribute("required");

      if (
        pressure.hasAttribute("required") ||
        bodyMassIndex.hasAttribute("required") ||
        heart.hasAttribute("required") ||
        age.hasAttribute("required")
      ) {
        break;
      } else {
        pressure.setAttribute("required", "required");
        bodyMassIndex.setAttribute("required", "required");
        heart.setAttribute("required", "required");
        age.setAttribute("required", "required");
        break;
      }

    case "dentist":
      dentist.classList.remove("d-none");
      cardiologist.classList.add("d-none");
      therapist.classList.add("d-none");
      age.removeAttribute("required");
      pressure.removeAttribute("required");
      bodyMassIndex.removeAttribute("required");
      heart.removeAttribute("required");

      if (lastVisit.hasAttribute("required")) {
        break;
      } else {
        lastVisit.setAttribute("required", "required");
        break;
      }

    case "therapist":
      therapist.classList.remove("d-none");
      cardiologist.classList.add("d-none");
      dentist.classList.add("d-none");
      pressure.removeAttribute("required");
      bodyMassIndex.removeAttribute("required");
      heart.removeAttribute("required");
      lastVisit.removeAttribute("required");

    default:
      break;
  }
});

// create card
async function createCard() {
  try {
    let response = await fetch("https://ajax.test-danit.com/api/v2/cards", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        doctor: `${doctor.value}`,
        purpose: `${visitForm.purpose.value}`,
        description: `${visitForm.description.value}`,
        priority: `${visitForm.priority.value}`,
        name: `${visitForm.name.value}`,
        pressure: `${visitForm.pressure.value}`,
        bodyMassIndex: `${visitForm.bodyMassIndex.value}`,
        heart: `${visitForm.heart.value}`,
        age: `${visitForm.age.value}`,
        lastVisit: `${visitForm.lastVisit.value}`,
      }),
    });

    let card = await response.json();

    viewAllCards();
  } catch (err) {
    alert(err.message);
  }
}

visitForm.addEventListener("submit", (el) => {
  el.preventDefault();
  createCard();
  let modal = document.getElementById("create-visit");
  let modalInstance = bootstrap.Modal.getInstance(modal); // Отримати екземпляр модального окна
  modalInstance.hide();
  visitForm.reset();
});

// view all cards
async function viewAllCards() {
  let response = await fetch("https://ajax.test-danit.com/api/v2/cards", {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  let cards = await response.json();

  const loginBtn = document.querySelector(".header__btn-login");
  const createBtn = document.querySelector(".header__btn-create-visit");
  const noitems = document.querySelector(".main__items-text");
  const welcome = document.querySelector(".main__text-wrapper");
  const cardsWrapper = document.querySelector(".main__cards-wrapper");

  loginBtn.classList.add("d-none");
  createBtn.classList.remove("d-none");
  noitems.classList.remove("d-none");
  welcome.classList.add("d-none");
  cardsWrapper.classList.remove("d-none");

  if (cards.length) {
    noitems.classList.add("d-none");
  }

  cardsWrapper.innerHTML = "";

  cards.forEach((card) => {
    if (card.doctor === "cardiologist") {
      const visitCardiologist = new VisitCardiologist(
        card.doctor,
        card.purpose,
        card.description,
        card.priority,
        card.name,
        card.id,
        card.pressure,
        card.bodyMassIndex,
        card.heart,
        card.age
      );
      const cardElement = visitCardiologist.render();
      cardElement.setAttribute("data-id", card.id);
      cardsWrapper.append(cardElement);
    } else if (card.doctor === "dentist") {
      const visitDentist = new VisitDentist(
        card.doctor,
        card.purpose,
        card.description,
        card.priority,
        card.name,
        card.id,
        card.lastVisit
      );
      const cardElement = visitDentist.render();
      cardElement.setAttribute("data-id", card.id);
      cardsWrapper.append(cardElement);
    } else if (card.doctor === "therapist") {
      const visitTherapist = new VisitTherapist(
        card.doctor,
        card.purpose,
        card.description,
        card.priority,
        card.name,
        card.id,
        card.age
      );
      const cardElement = visitTherapist.render();
      cardElement.setAttribute("data-id", card.id);
      cardsWrapper.append(cardElement);
    } else {
      const visit = new Visit(
        card.doctor,
        card.purpose,
        card.description,
        card.priority,
        card.name,
        card.id
      );
      const cardElement = visit.render();
      cardElement.setAttribute("data-id", card.id);
      cardsWrapper.append(cardElement);
    }
  });
}

//add filters

const searchInput = document.querySelector('input[type="search"]');
const selectPriority = document.getElementById("selectPriority");

searchInput.addEventListener("input", handleFilterChange);
selectPriority.addEventListener("change", handleFilterChange);

function handleFilterChange() {
  const searchValue = searchInput.value.trim().toLowerCase();
  const priorityFilter = selectPriority.value.toLowerCase();

  if (searchValue === "" && priorityFilter === "choose priority") {
    viewAllCards();
  } else {
    viewFilteredCards(searchValue, priorityFilter);
  }
}

async function viewFilteredCards(searchValue, priorityFilter) {
  let response = await fetch("https://ajax.test-danit.com/api/v2/cards", {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  let cards = await response.json();

  if (cards.length) {
    document.querySelector(".main__items-text").classList.add("d-none");
  } else {
    document.querySelector(".main__items-text").classList.remove("d-none");
  }

  const cardsWrapper = document.querySelector(".main__cards-wrapper");
  cardsWrapper.innerHTML = "";

  cards
    .filter((card) => {
      return (
        (card.name.toLowerCase().includes(searchValue) ||
          card.doctor.toLowerCase().includes(searchValue) ||
          card.purpose.toLowerCase().includes(searchValue) ||
          card.description.toLowerCase().includes(searchValue)) &&
        (priorityFilter === "choose priority" ||
          card.priority.toLowerCase() === priorityFilter)
      );
    })
    .forEach((card) => {
      let cardElement;
      if (card.doctor === "cardiologist") {
        const visitCardiologist = new VisitCardiologist(
          card.doctor,
          card.purpose,
          card.description,
          card.priority,
          card.name,
          card.id,
          card.pressure,
          card.bodyMassIndex,
          card.heart,
          card.age
        );
        cardElement = visitCardiologist.render();
      } else if (card.doctor === "dentist") {
        const visitDentist = new VisitDentist(
          card.doctor,
          card.purpose,
          card.description,
          card.priority,
          card.name,
          card.id,
          card.lastVisit
        );
        cardElement = visitDentist.render();
      } else if (card.doctor === "therapist") {
        const visitTherapist = new VisitTherapist(
          card.doctor,
          card.purpose,
          card.description,
          card.priority,
          card.name,
          card.id,
          card.age
        );
        cardElement = visitTherapist.render();
      }
      cardElement.setAttribute("data-id", card.id);
      cardsWrapper.append(cardElement);
    });
}
